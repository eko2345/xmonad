import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.NoBorders
import XMonad.Layout.Gaps
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO



main = do
  xmproc <- spawnPipe "xmobar"
  xmonad $ docks defaultConfig
    { modMask = mod4Mask
    , terminal = "kitty"
    , startupHook = startup'
    , borderWidth = 1
    , focusedBorderColor = "#698b69"
    , manageHook = manageDocks <+> manageHook defaultConfig
    , layoutHook = avoidStruts $ layoutHook defaultConfig
    , logHook = dynamicLogWithPP $ xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        ,  ppTitle = xmobarColor "green" "" . shorten 50
                        }
    } `additionalKeys` myKeys
  where  
    startup' = do
      spawn "/home/juicyjouissance/.screenlayout/l.sh"

    myKeys =  
      [((mod4Mask .|. shiftMask, xK_e), spawn "emacs")
      ,((mod4Mask .|. shiftMask, xK_f), spawn "firefox")
      ,((mod4Mask .|. shiftMask, xK_d), spawn "discord")
      ]
    
